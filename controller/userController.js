const User = require("../model/User");
const Product = require("../model/Product");
const bcrypt = require("bcrypt");
const auth = require ("../auth");

// S42 ---~~~---~~~---~~~---~~~---~~~---~~~`
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		email : reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	}) 
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
const passwordMatch = bcrypt.compareSync(reqBody.password, result.password);
		
		if(result == null){
			return false;
		} else if (passwordMatch) {
				return {access : auth.createAccessToken(result)}
			} else {
				return false;
			}
	})
}

// S45 ---~~~---~~~---~~~---~~~---~~~---~~~`
// non admin checkout

module.exports.userCheckout = async(data) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {
        if(!user.orders[0]) {
            user.orders.push({products: []});
        }
        user.orders[0].products.push({productId: data.productId, 
                productName: data.productName, 
                quantity: data.quantity});

        return user.save().then((user, error) => {
            if (error) {
                return false;
            } else {
                return true;
            }
        })
    })
    let isProductUpdated = await Product.findById(data.productId).then(product => {
        product.orders.push({orderId : data.orderId, 
                productName: data.productName, 
                quantity: data.quantity})
        return product.save().then((product, error) => {
            if (error) {
                return false;
            } else {
                return true;
            }
        })
    })
    if (isUserUpdated && isProductUpdated) {
        return true;
    } else {
        return false;
    } 
}



// retrieve user details
module.exports.getProfile = (reqBody) => {
    return User.findById(reqBody.userId).then(result => {
        result.password = "";
        return result;

    });

};



module.exports.setAdminStatus = (userId, isAdmin) => {
  return new Promise((resolve, reject) => {
    User.findByIdAndUpdate(userId, { isAdmin: isAdmin }, { new: true }, (error, user) => {
      if (error) {
        reject(error);
      }
      resolve(user);
    });
  });
};

// module.exports = {
//   async getAllOrders(req, res) {
//     try {
//       const users = await User.find();
//       const orders = users.map(user => user.orders).flat();
//       res.status(200).json(orders);
//     } catch (err) {
//       console.error(err);
//       res.status(500).json({ message: "Internal server error" });
//     }
//   }
// };

module.exports.getAllOrders = (req, res) => {
   return User.find()
      .then(users => {
        const orders = users.map(user => user.orders).flat();
        res.status(200).json(orders);
      })
      // .catch(err => {
      //   console.error(err);
      //   res.status(500).json({ message: "Internal server error" });
      // });
  
}

module.exports.getAllOrders = async (req, res) => {
  
    const users = await User.find({});
    const orders = [];

    for (const user of users) {
      for (const order of user.orders) {
        const orderDetails = {
          userEmail: user.email,
          products: [],
          totalAmount: order.totalAmount,
          purchasedOn: order.purchasedOn
        };

        for (const item of order.products) {
          const product = await Product.findById(item.productId);
          if (product) {
            const productDetails = {
              productId: product._id,
              productName: product.name,
              quantity: item.quantity,
              price: product.price
            };
            orderDetails.products.push(productDetails);
          }
        }
        orders.push(orderDetails);
      }
    }
    return orders;
  } 

