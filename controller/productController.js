const Product = require("../model/Product");
const cloudinary = require('../others/Cloudinary')


// S43 ---~~~---~~~---~~~---~~~---~~~---~~~`
// Create a Product 

module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {
		return result;
	})
}

// module.exports.addProduct = (productDetail) => {

//     let newProduct = new Product ({
//         name: productDetail.product.name,
//         description: productDetail.product.description,
//         price: productDetail.product.price        
//     });
//     return newProduct.save().then((product, error) => {
       
//         if(error){
//             return false;
        
//         } else{
//             return true;
//         }
//     }); 
    
// };

module.exports.addProduct = async (req, res) => {
  
	// get the product data from the request body
	const { name, description, price, image } = req.body;
   
	try {
       const result = await cloudinary.uploader.upload(image, {
		folder: "products"
		// width 300,
		// crop: "scale"
	   })
	   const product = await Product.create({
		name,
		description,
		price,
		isActive: true,
		image:{
			public_id: result.public_id,
			url: result.secure_url
		}
	   })
       res.status(201).json({
		success: true,
		product
	   })
     } catch (error) {
       console.error(error);
       // send an error response
       res.status(500).json({ error: 'Failed to add product' });
     }
   };

// Retrieve all active Products

module.exports.viewAllActiveProducts = () => {
	return Product.find({isActive : true}).then(result => {
		return result
	});
};

// S43 ---~~~---~~~---~~~---~~~---~~~---~~~`
// retrieve a single product

/*module.exports.searchProduct = (reqBody) => {
	return Product.find({ name: { $regex: new RegExp(reqBody.name, "i") } }).then((result) => {
		if (result.length > 0){
			return result
		} else {
			return (`${reqBody.name} is not found in database.`)
		}
		
	});
};*/
module.exports.searchProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    })
}



module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	// Syntax: findbyIdAndUpdate(documents ID, updatesToBeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {


		if (error) {
			return false;

	
		} else {
			return true
		}
	})
};

// Archiving

module.exports.archiveProduct = (reqParams, reqBody) =>{

	let updateActiveField = {
		isActive : reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product) => {
		return true;
	}).catch((error) => {
		return false;
	});
};


module.exports.addToCart = async (params, body) => {
  const product = await Product.findById(params.productId);
  if (!product) throw new Error(false);

  const order = {
    orderId: body.orderId,
  };
  product.orders.push(order);
  await product.save();
  return true;
};

exports.getOrders = async (productId) => {
  const product = await Product.findById(productId);
  if (!product) {
    throw new Error("Product not found");
  }
  const orders = product.orders;
  return { orders };
};
