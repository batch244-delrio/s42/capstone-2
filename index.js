const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require('body-parser');
const app = express();
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.zoejmfh.mongodb.net/Ecommerse_API?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use("/users", userRoutes);
app.use("/", productRoutes);


app.listen(process.env.PORT || 4000, () => {
  console.log(`API is now online at port ${process.env.PORT || 4000}`);
});






