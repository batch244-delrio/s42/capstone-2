const User = require("../model/User");
const Product = require("../model/Product");
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require ("../controller/userController");




 
// S42 ---~~~---~~~---~~~---~~~---~~~---~~~`

// User Registation
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// user authentication

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// S45 ---~~~---~~~---~~~---~~~---~~~---~~~`
// non admin checkout

router.post("/checkout",auth.verify, (req, res) => {
	const userData = {
		userId : auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
		
	if (!userData.isAdmin) {	
		userController.userCheckout(userData).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});






// retrieve user details

router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);
  userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});




router.put("/setAdminStatus/:userId", auth.verify, (req, res) => {
  const requesterId = auth.decode(req.headers.authorization).id;
  const targetUserId = req.params.userId;
  const isAdmin = req.body.isAdmin;

  const decoded = auth.decode(req.headers.authorization);
  if (decoded.isAdmin) {
    userController.setAdminStatus(targetUserId, isAdmin)
      .then((result) => res.send(result))
      .catch((error) => res.status(500).send({ error: error.message }));
  } else {
    res.status(401).send(false);
  }
});

router.get("/orders", async (req, res) => {
  try {
    const users = await User.find();
    const orders = users.map(user => user.orders).flat();
    res.status(200).json(orders);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Internal server error" });
  }
});

router.delete("/cart/:productId", (req, res) => {
	userController.removeProduct(req.params.id).then(resultFromController => res.send(resultFromController));
});


router.get("/getOrders", (req, res) => {
  userController.getAllOrders().then(resultFromController => res.send(resultFromController))
} ) ;




module.exports = router;