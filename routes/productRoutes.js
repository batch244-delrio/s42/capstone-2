const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require ("../controller/productController");
const Product = require("../model/Product");
const User = require("../model/User");


// S43 ---~~~---~~~---~~~---~~~---~~~---~~~`
// create a product by admin only

router.get("/allProducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// add new product
router.post('/createProd', async (req, res) => {
  const data = req.body;
  const resultFromController = await productController.addProduct(data);
  res.send(resultFromController);
});




// Retrieving all active products
router.get("/activeProducts", (req, res) => {
	productController.viewAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// S44 ---~~~---~~~---~~~---~~~---~~~---~~~`
// Retrieving a single Product
router.get("/:productId", (req, res) => {
	productController.searchProduct(req.params).then(resultFromController => res.send(resultFromController));
} );

// Update Product information (Admin)

router.put("/:productId", auth.verify, (req, res) => {

	// To know if the logged in user is an admin or not
	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));

	// User is not an admin
	} else {
		res.send(false)
	}
	
});

// Archiving

router.patch("/:productId/archive", auth.verify, (req, res) => {

	// To know if the logged in user is an admin or not
	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});









router.post("/addToCart/:productId", async (req, res) => {
  try {
    const result = await productController.addToCart(req.params, req.body);
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

router.get("/:productId/orders", (req, res) => {
  productController.getOrders(req.params.productId)
    .then(resultFromController => res.send(resultFromController))
});







module.exports = router;