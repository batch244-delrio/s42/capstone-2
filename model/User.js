const mongoose = require("mongoose")
const userSchema = new mongoose.Schema(
		{
			email: {
				type: String,
				required: [true, "Please input your E-mail."]
			},
			password: {
				type: String,
				required: [true, "Please input your password."]
			},
			isAdmin: {
				type: Boolean,
				default: false
			},
			orders: [{
				products: [{
					productId: {
						type: String
					},	
					productName: {
						type: String		
					},
					quantity: {
						type: Number
						}
					}],
			totalAmount: {
				type: Number		
				},
			purchasedOn: {
				type: Date,
				default: new Date
				}
			}
		]
	}

);

module.exports = mongoose.model("User", userSchema);