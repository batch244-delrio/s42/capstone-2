const mongoose = require("mongoose")
const productSchema = new mongoose.Schema(
		{
			name: {
				type: String,
				required: [true, "Please input Product Name."]
			},
			description: {
				type: String,
				required: [true, "Please input description."]
			},
			price: {
				type: Number,
			},
			isActive: {
				type: Boolean,
				default: true
			},
			image : {
				public_id: {
					type: String,
					required: true
				},
				url: {
					type: String,
					required: true
				}
			},
			createdOn: {
				type: Date,
				default: new Date
			},
			orders: [
				{					
					orderId: {
						type: String
					},
					
					quantity: {
						type: Number
					}
						
					 
					
				}
			]
		}

	);

module.exports = mongoose.model("Product", productSchema); 